# Minecraft Server Manager

## INFO
This script is created for private servers or local servers based on Linux.
The script allows us to quickly install Minecraft server.

## CHANGLELOG
0.1A
```
- Add to git
```
0.2A
```
- Add reinstall
```
0.3A
```
- Add uninstall
- Fix Syntax
```
## INSTALLATION
Make sure you have installed git

```bash
git clone https://github.com/UkasikPL/Minecraft_Server_Manager.git
```
## USAGE
### for install
```shell
./mc_server_manager.sh
```
### for start server
```shell
./mc_server_manager.sh start
```
### for reinstall
```shell
./mc_server_manager.sh reinstall
```
### for uninstall
```shell
./mc_server_manager.sh uninstall
```
##### Script created by Kacper �ukasik
